package com.example.boilerplate

import com.example.boilerplate.data.CarListApi
import com.example.boilerplate.data.CarListDataSource
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Test

class CarListDataSourceTest : BaseTest() {

    @MockK(relaxed = true)
    lateinit var api: CarListApi

    @InjectMockKs
    lateinit var dataSource: CarListDataSource

    override fun setUp() {
        coEvery { dataSource.fetchCarList() } returns listOf()
    }

    @Test
    fun `when fetchCarList is called, should return correct data`() = runBlocking {
        // Given

        // When
        dataSource.fetchCarList()

        // Then
        coVerify { api.fetchCarList() }
    }
}
