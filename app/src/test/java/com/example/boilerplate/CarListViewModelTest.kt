package com.example.boilerplate

import android.app.Application
import android.content.res.Resources
import com.example.boilerplate.data.CarListRepository
import com.example.boilerplate.scene.cars.CarListViewModel
import com.example.boilerplate.scene.cars.model.CarUIModel
import com.google.common.truth.Truth
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Test

class CarListViewModelTest : BaseTest() {
    private lateinit var viewModel: CarListViewModel

    @MockK(relaxed = true)
    private lateinit var resources: Resources

    @MockK
    private lateinit var carListRepository: CarListRepository

    @MockK(relaxed = true)
    private lateinit var application: Application

    override fun setUp() {
        viewModel = CarListViewModel(
            application,
            carListRepository
        )
        every { application.resources } returns resources
    }

    @Test
    fun `when viewModel initialised set a value to the carList`() {
        // Given
        coEvery { carListRepository.fetchCarList() } returns listOf(CarUIModel())
        // When
        viewModel.fetchCarList()
        val actual = viewModel.carList.getOrAwaitValue()

        // Then
        Truth.assertThat(actual).isNotEmpty()
    }
}
