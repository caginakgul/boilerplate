package com.example.boilerplate

import com.example.boilerplate.data.CarListDataSource
import com.example.boilerplate.data.CarListRepository
import com.example.boilerplate.data.mapper.CarUIMapper
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Test

class CarListRepositoryTest : BaseTest() {

    @MockK(relaxed = true)
    lateinit var dataSource: CarListDataSource

    @MockK(relaxed = true)
    lateinit var mapper: CarUIMapper

    @InjectMockKs
    lateinit var repository: CarListRepository

    override fun setUp() {
        coEvery { dataSource.fetchCarList() } returns listOf()
    }

    @Test
    fun `when fetchCarList is called, repository should return a data`() = runBlocking {
        // Given

        // When
        repository.fetchCarList()

        // Then
        coVerify { dataSource.fetchCarList() }
    }
}
