package com.example.boilerplate.scene.base

interface ListAdapterItem {

        val id: String

        override fun equals(other: Any?): Boolean
        override fun hashCode(): Int
}
