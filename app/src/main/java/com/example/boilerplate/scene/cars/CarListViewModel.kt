package com.example.boilerplate.scene.cars

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.boilerplate.data.CarListRepository
import com.example.boilerplate.scene.base.BaseViewModel
import com.example.boilerplate.scene.cars.model.CarUIModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class CarListViewModel @Inject constructor(
    app: Application,
    private val carListRepository: CarListRepository
) : BaseViewModel(app) {

    private val _carList = MutableLiveData<List<CarUIModel>>()
    val carList: LiveData<List<CarUIModel>>
        get() = _carList

    fun fetchCarList() {
        viewModelScope.launch {
            try {
                _carList.value = carListRepository.fetchCarList()
            } catch (e: IOException) {
                handleFailure(e)
            }
        }
    }
}
