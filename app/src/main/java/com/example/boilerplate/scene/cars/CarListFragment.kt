package com.example.boilerplate.scene.cars

import android.os.Bundle
import android.view.View
import com.example.boilerplate.R
import com.example.boilerplate.databinding.FragmentCarListBinding
import com.example.boilerplate.scene.base.BaseFragment
import com.example.boilerplate.scene.cars.model.CarUIModel
import com.example.boilerplate.util.bitmapDescriptorFromVector
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class CarListFragment : BaseFragment<CarListViewModel, FragmentCarListBinding>(
    R.layout.fragment_car_list
), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private lateinit var carList: List<CarUIModel>
    private var isDataReady: Boolean = false
    private var isMapReady: Boolean = false

    override fun initialize() {
        super.initialize()
        binder.recyclerViewCarList.adapter = CarAdapter()
        binder.map.getMapAsync(this)
        viewModel.fetchCarList()
    }

    override fun observeData() {
        super.observeData()
        viewModel.carList.observe(viewLifecycleOwner) {
            Timber.i("Data is received to UI")
            carList = it
            isDataReady = true
            checkDataIsReady()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binder) {
            map.onCreate(savedInstanceState)
            map.onResume()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        Timber.i("Map is ready")
        map = googleMap
        isMapReady = true
        checkDataIsReady()
    }

    private fun checkDataIsReady() {
        if (isDataReady && isMapReady) {
            setMarkers()
        }
    }

    private fun setMarkers() {
        Timber.i("Markers are setting")
        for (car in carList) {
            val carLocation = LatLng(car.latitude, car.longitude)
            val marker = map.addMarker(
                MarkerOptions()
                    .icon(
                        requireContext().bitmapDescriptorFromVector(
                            R.drawable.ic_car
                        )
                    )
                    .position(carLocation).title(car.licensePlate)
            )
            marker?.tag = car.id
        }
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    MUNICH_LAT, MUNICH_LNG
                ), ZOOM_LEVEL
            )
        )
    }

    companion object {
        private const val MUNICH_LAT = 48.137154
        private const val MUNICH_LNG = 11.576124
        private const val ZOOM_LEVEL = 12f
    }
}
