package com.example.boilerplate.scene.cars

import com.example.boilerplate.R
import com.example.boilerplate.databinding.ItemCarBinding
import com.example.boilerplate.scene.base.BaseListAdapter
import com.example.boilerplate.scene.cars.model.CarUIModel

class CarAdapter : BaseListAdapter<ItemCarBinding, CarUIModel>() {
    override fun getLayoutId(position: Int): Int = R.layout.item_car

    override fun bind(binding: ItemCarBinding, item: CarUIModel, position: Int) {
        binding.model = item

        with(binding) {
            executePendingBindings()
        }
    }
}
