package com.example.boilerplate.scene.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

abstract class BaseListAdapter<VB : ViewDataBinding, T : ListAdapterItem>(
    diffCallback: DiffUtil.ItemCallback<T> = ListAdapterItemDiffCallback()
) : ListAdapter<T, BaseViewHolder<VB>>(diffCallback) {

    private val removedItems = arrayListOf<T>()
    lateinit var binding: VB

    @LayoutRes
    protected abstract fun getLayoutId(position: Int): Int

    protected abstract fun bind(binding: VB, item: T, position: Int)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        @LayoutRes viewType: Int
    ): BaseViewHolder<VB> {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            viewType,
            parent,
            false
        )

        return BaseViewHolder(binding)
    }

    override fun getItemViewType(position: Int): Int = getLayoutId(position)

    override fun onBindViewHolder(holder: BaseViewHolder<VB>, position: Int) {
        bind(holder.binding, getItem(position), position)
    }

    @CallSuper
    override fun submitList(list: List<T>?) {
        submit(list)
    }

    private fun submit(list: List<T>?) {
        removedItems.clear()
        super.submitList(list)
    }
}
