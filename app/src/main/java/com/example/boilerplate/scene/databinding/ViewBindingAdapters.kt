package com.example.boilerplate.scene.databinding

import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.boilerplate.R
import com.example.boilerplate.scene.base.BaseListAdapter
import com.example.boilerplate.scene.base.ListAdapterItem
import com.example.boilerplate.util.loadImage

@Suppress("UNCHECKED_CAST")
@BindingAdapter("submitList")
fun submitList(view: RecyclerView, list: List<ListAdapterItem>?) {
    val adapter = view.adapter as BaseListAdapter<ViewDataBinding, ListAdapterItem>?
    adapter?.submitList(list?.let { ArrayList(it) })
}

@BindingAdapter("imageFromUrl", "placeholderRes", requireAll = false)
fun setImage(
    view: ImageView,
    url: String?,
    @DrawableRes placeholderRes: Int?
) {
    val defaultDrawable = R.drawable.ic_car
    view.loadImage(
        url,
        placeholderRes ?: defaultDrawable
    )
}
