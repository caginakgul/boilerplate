package com.example.boilerplate.scene.main

import android.os.Bundle
import com.example.boilerplate.R
import com.example.boilerplate.databinding.ActivityMainBinding
import com.example.boilerplate.scene.base.BaseBindingActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseBindingActivity<MainViewModel, ActivityMainBinding>() {

    override val layoutId get() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binder.viewModel = viewModel
        binder.lifecycleOwner = this
    }
}
