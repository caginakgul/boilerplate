package com.example.boilerplate.data

import com.example.boilerplate.data.model.CarResponse
import retrofit2.http.GET

interface CarListApi {
    @GET(CARS)
    suspend fun fetchCarList(): List<CarResponse>

    companion object {
        const val CARS = "cars"
    }
}
