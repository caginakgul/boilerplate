package com.example.boilerplate.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class CarResponse(
    @Json(name = "id") val id: String,
    @Json(name = "modelIdentifier") val modelIdentifier: String,
    @Json(name = "modelName") val modelName: String,
    @Json(name = "name") val name: String,
    @Json(name = "make") val make: String,
    @Json(name = "group") val group: String,
    @Json(name = "color") val color: String,
    @Json(name = "series") val series: String,
    @Json(name = "fuelType") val fuelType: String,
    @Json(name = "fuelLevel") val fuelLevel: Double,
    @Json(name = "transmission") val transmission: String,
    @Json(name = "licensePlate") val licensePlate: String,
    @Json(name = "latitude") val latitude: Double,
    @Json(name = "longitude") val longitude: Double,
    @Json(name = "innerCleanliness") val innerCleanliness: String,
    @Json(name = "carImageUrl") val carImageUrl: String,
) : Parcelable
