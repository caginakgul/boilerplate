package com.example.boilerplate.data.mapper

import com.example.boilerplate.data.model.CarResponse
import com.example.boilerplate.scene.cars.model.CarUIModel
import javax.inject.Inject

class CarUIMapper @Inject constructor() {

    fun toUiModel(responseModel: CarResponse) = with(responseModel) {
        CarUIModel(
            id = id,
            modelIdentifier = modelIdentifier,
            modelName = modelName,
            name = name,
            make = make,
            group = group,
            color = color,
            series = series,
            fuelType = fuelType,
            fuelLevel = fuelLevel,
            transmission = setTransmission(transmission),
            licensePlate = licensePlate,
            latitude = latitude,
            longitude = longitude,
            innerCleanliness = innerCleanliness,
            carImageUrl = carImageUrl
        )
    }

    private fun setTransmission(transmissionType: String): String {
        return if (transmissionType == Manual) {
            Transmission.Manual.name
        } else {
            Transmission.Automatic.name
        }
    }

    enum class Transmission {
        Manual,
        Automatic
    }

    companion object {
        private const val Manual = "M"
    }
}
