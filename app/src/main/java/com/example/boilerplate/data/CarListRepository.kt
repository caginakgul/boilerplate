package com.example.boilerplate.data

import com.example.boilerplate.data.mapper.CarUIMapper
import com.example.boilerplate.scene.cars.model.CarUIModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CarListRepository @Inject constructor(
    private val mapper: CarUIMapper,
    private val carListDataSource: CarListDataSource
) {
    suspend fun fetchCarList(): List<CarUIModel> {
        val data = carListDataSource.fetchCarList()
        return data.map { mapper.toUiModel(it) }
    }
}
