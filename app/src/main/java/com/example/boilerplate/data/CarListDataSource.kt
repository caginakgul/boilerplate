package com.example.boilerplate.data

import com.example.boilerplate.data.model.CarResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CarListDataSource @Inject constructor(
    private val api: CarListApi,
) {

    suspend fun fetchCarList(): List<CarResponse> {
        return api.fetchCarList()
    }
}
